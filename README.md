# BIOVIT Git tutorial

## Description
This repository contains slides and files for the Git tutorial at the [BIOVIT faculty](https://www.nmbu.no/en/faculties/biosciences).

## Version overview

The first version of this tutorial was held on the 15th of May 2024

## Contact

For questions and requests, contact Simeon Lim Rossmann at [simeon.rossmann@nmbu.no](mailto:simeon.rossmann@nmbu.no) or [open an issue](https://gitlab.com/nmbu.no/workshops/git/biovit/-/issues/new).

## License
[Artistic License 2.0](https://opensource.org/license/artistic-2-0)
